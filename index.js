// [SECTION] Comparison Query Operators

	//$gt/$gte operator
	/*
		-allows us to find documents that have field values greater than of equal specified value

		-syntax
			db.collectionName.find({field: {$gt: value}})


	*/

	//will return not inc 65
	db.users.find({age:{ $gt:65}})
	//will return age = 65
	db.users.find({age:{ $gte:65}});

	//$lt/$lte operator
	/*
		-allows us to find documents that have field values greater than of equal specified value

		-syntax
			db.collectionName.find({field: {$lt: value}})


	*/
	//lesthen 65
	db.users.find({age:{ $lt:65}})
	//will return age <= 65
	db.users.find({age:{ $lte:65}});
	//$ne operator
		/*
			-allows us to find document that have field number values not equal to a specified value
			-syntax:
				db.collectionName.find({field: {$ne:{value}}})

		*/

	db.users.find({age:{ $ne: 82}})
	// $in operator 

	/*
		-allows us to find documents with specific match criteria of one field using diff values
		-Syntax:
			db.collectionName.find({field:{ $in: {[valueA, valueB]}}})

	*/


	db.users.find({lastName:{ $in:["Hawking", "Doe"]}  )
	db.users.find({courses:{ $in:["HTML", "React"]}  )
//[SECTION]

		//$or operator
			/*
				-allow us to find documents that match a single criteria from multiple provided search criteria

				-Syntax:
					db.collectionName.find({$or: [{fieldA:valueA}, {fieldB:valueB}]})
			*/
	db.users.find({
		$or:[
				{firstName: "Neil"},
				{age: 25},
			]
	})
//
		
		db.users.find({
		$or:[
				{firstName: "Jane"},
				{age: {$gt: 30}}
			]
	})

	//$and

		/*
			-allows us to find documents matching multiple criteria in a single field



		*/

			db.users.find({
		$and:[
				{age: {$ne: 82}},
				{age: {$ne: 76}}
			]
	})

	//mini activity
					db.users.find({
		$and:[
				{courses:{ $in:["Laravel", "React"]},

				{age: {$lte: 82}}
				
			]
	})

db.users.find({
$and:[
		{courses:{ $in:["React", "Laravel"]},

		{age: {$lte: 80}}
				
     ]
})

//[SECTION] Field projection

	//To help with readability of the values returned, we can include/exclude field from the retrieve results.


	//Inclusion
	/*
		-allows us to include/add specific fields only when retrieving documents
		-The value provided is 1 to denote that field being included.
		-Syntax:
			db.users.find({criteria}, {field: 1})

	*/

	db.users.find(
			{
				firstName: "Jane"
			},
			{
				firstName: 1,
				lastName: 1,
				contact: 1
			}
		)

	//Exclusion
	/*
		-allows us to exclude specific fields only when retrieving documents.
		-the value provided is 0 to denote that the field is being excluded

		Syntax:
		db.users.find({criteria}, {})
	*/
	
	db.users.find(
			{
				firstName: "Jane"
			},
			{
				_id: 0,
				contact: 0,
				department: 0

			}
		)

	//return a embedded documents

	db.users.find(
			{
				firstName: 1,
				lastName: 1,
				"contact.phone": 1
			}		
		)

	//exclude
		db.users.find(
			{ firstName: "Jane"},
                        {
				
				"contact.phone": 0
			}		
		);

	//Project Specific Elements in the Returned Array.
		//The $slice operator allows us to retrieve element that matches the criteria.
		// Syntax:
			//db.collection.find({criteria}, arrayField: {$slice: count})

		db.users.find(
				{
					firstName: "Jane"
				},
				{
					courses: {$slice :1}
				}
			)
//[SECTION] Evaluation Query operator 
		db.users.find({firstName: "jane"})

		//$regex operator
		/*
			-allows us to find documents that a specific string pattern using"regular expression"/"regex".

			-Syntax:
				db.collectionName.find({field: $regex: "pattern" $options: "optionValue"})
		*/

	//case sensitive query

		db.users.find({firstname:{$regex: "j"}})







